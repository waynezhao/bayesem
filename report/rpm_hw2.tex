\documentclass[]{article}
\usepackage{enumerate}
\usepackage{amsmath}
\usepackage{url}
\usepackage{graphicx}
\usepackage[pdf]{graphviz}
% new command
\newcommand\numberthis{\addtocounter{equation}{1}\tag{\theequation}}
\begin{document}

\title{Bayesian Network parameter learning from incomplete data using Expectation Maximization}
\author{Weiran Zhao (zhaoweir@indiana.edu)}
\date{Thursday, Apr 3rd, 2014}
\maketitle
\section{Problem Description}
Write a program that reads a specification of a Bayesian network, and a query in the format described below and runs the Expectation Maximization (EM) algorithm to estimate the conditional probability distributions. To keep things simple all variables in our networks will be binary. Let the Bayesian network contain 4 nodes: cold, sneeze, allergy and a cat in the house.

The structure of the Bayesian network is as follows: cold and allergy are parents of sneeze and cat is the parent of allergy. This is to say that sneeze could be caused by some combination of allergy and cold and presence of a cat causes allergy.

Given the following data, compute the appropriate probability distributions. Identify the conditional and prior distributions and learn the parameters through EM.
\begin{table}[h]
\centering
\begin{tabular}{|c|c|c|c|}
\hline
cat & allergy & cold & sneeze \\
\hline
Y & N & N & N \\
\hline
? & Y & Y & Y \\
\hline
Y & Y & N & N \\
\hline
N & Y & N & Y \\
\hline
Y & N & ? & Y \\
\hline
N & N & N & N \\
\hline
Y & Y & Y & Y \\
\hline
N & Y & Y & N \\
\hline
N & Y & Y & N \\
\hline
Y & Y & Y & N \\
\hline
N & ? & Y & N \\
\hline
Y & Y & N & Y \\
\hline
Y & Y & N & Y \\
\hline
N & N & Y & Y \\
\hline
N & N & N & N \\
\hline
\end{tabular}
\end{table}
\begin{enumerate}[1]
\item In the above data Y refers to yes, N refers to no, and ? refers to hidden. Compute the parameters of the Bayesian Network. (30 points)
\item Assume that now we observe the following: sneeze is true (sneeze = yes), cold is not present (cold = no). The goal is to infer if a cat is present in the house. Write out the equations to compute the probabilities. (Hint, use Bayes rule and compute $P(cat = yes | cold = no, sneeze = yes)$. You have to sum out allergy). Show clearly the calculation. (7 points)
\item Now use the estimated parameters from the first part and compute the posterior probability of a cat being present (3 points).
\end{enumerate}
\section{Proposed Solution}
First of all, to help understanding, the structure of Bayesian Network for this problem has been given in Figure~\ref{figBayesNet}
%% Start of dot diagram
\begin{figure}[htbp]
\centering
\digraph[scale=1.0]{bayesNet}{
cold->sneeze;
cat->allergy;
allergy->sneeze
}
\caption{Bayesian Network representation for this problem}
\label{figBayesNet}
\end{figure}
%% End of dot diagram
% to Q1
\subsection{Q1: Parameters learning}
I am only listing my program result here, as for how to run the program, please check Appendix.
\begin{figure}[htbp]
\centering
\includegraphics[scale=0.55]{progResult}
\end{figure}
\subsection{Q2: Query}
First, let's adopt the following short notation, e.g. $P(ca=Y)$ stands for $P(cat = yes)$, $P(sn=N)$ for $P(sneeze = no)$ and etc.

What we want to know is 
\begin{equation}
P(ca=Y | sn=Y, co=N) = \frac{P(ca=Y, sn=Y, co=N)}{P(sn=Y, co=N)}
\label{eqcond}
\end{equation}

For numerator $P(ca=Y, sn=Y, co=N)$, we can calculate through,
\begin{align*}
& P(ca=Y, sn=Y, co=N) \\
= & \sum_{al} P(ca=Y, sn=Y, co=N, al) \\
= & \sum_{al} P(ca=Y) * P(al | ca=Y) * P(co=N) * P(sn=Y | co=N, al)\\
= & P(ca=Y) * P(co=N) * \{\sum_{al} P(al | ca = Y) * P(sn=Y | co=N, al)\} \\
= & P(ca=Y) * P(co=N) \\
& * \{P(al=Y | ca = Y) * P(sn=Y | co = N, al = Y) \\
& + P(al = N | ca = Y) * P(sn = Y | co = N, al = N)\} \numberthis \label{eqnumer}
\end{align*}

For denominator $P(sn=Y, co=N)$, we have
\begin{align*}
& P(sn=Y, co=N)  \\
= & \sum_{al} \sum_{ca} P(ca, sn=Y, co=N, al) \\
= & \sum_{al} \{ P(ca = Y, sn=Y, co=N, al) + P(ca = N, sn=Y, co=N, al)\} \\
= & P(ca = Y, sn=Y, co=N) + P(ca = N, sn=Y, co=N) \numberthis \label{eqdenom}
\end{align*}

We know how to calculate $P(ca = Y, sn=Y, co=N)$ in Equation~\eqref{eqdenom} from Equation~\eqref{eqnumer}. Similarly, $P(ca = N, sn=Y, co=N)$ can be calculated as, 
\begin{align*}
& P(ca=N, sn=Y, co=N) \\
= & \sum_{al} P(ca=N, sn=Y, co=N, al) \\
= & \sum_{al} P(ca=N) * P(al | ca=N) * P(co=N) * P(sn=Y | co=N, al)\\
= & P(ca=N) * P(co=N) * \{\sum_{al} P(al | ca = N) * P(sn=Y | co=N, al)\} \\
= & P(ca=N) * P(co=N) \\
& * \{P(al=Y | ca = N) * P(sn=Y | co = N, al = Y) \\
& + P(al = N | ca = N) * P(sn = Y | co = N, al = N)\} \numberthis \label{eqsimnumer}
\end{align*}

A Substitution of Equation~\eqref{eqnumer} ~\eqref{eqdenom} and ~\eqref{eqsimnumer} into Equation~\eqref{eqcond} will give us $P(ca=Y | sn=Y, co=N)$.
\subsection{Q3: Posterior}
According to my result in Q1, the numbers we need are:
\begin{align*}
P(ca=Y) & = 0.529 \\
P(ca=N) & = 0.471 \\
P(co=Y) & = 0.529 \\
P(co=N) & = 0.471 \\
P(al=Y|ca=Y) & = 0.7 \\
P(al=N|ca=Y) & = 0.3 \\
P(al=Y|ca=N) & = 0.556 \\
P(al=N|ca=N) & = 0.444\\
P(sn = Y|co=N,al=Y) &= 0.667 \\
P(sn=Y|co=N,al=N) &=0.2\\
\end{align*}
Plug in these number into Equation~\eqref{eqcond}~\eqref{eqnumer} ~\eqref{eqdenom} and ~\eqref{eqsimnumer} , we can get
\begin{equation*}
P(ca=Y|sn=Y,co=N) = 0.5633
\end{equation*}
\newpage
\section{Appendix}
\begin{verbatim}
====================
Modification Log: 
====================
Started: Thu,Apr 03th 2014 12:43:39 AM EDT
Last Modified: Thu,Apr 03th 2014 03:01:43 AM EDT

================
Program Input:
================
The input of program consists of three parts:
    ------------------------------------
    1. Bayesian Network Specification:
       EXAMPLE FILE: bayesNetSpecs
    ------------------------------------
    Which consists of three parts:
        a) NODE specification: [node's name] + [possible states of that node]
           E.g. cat N Y 
           One advantage of this program is that it supports multiple state of
           each random variable
        b) # separator: indicating switch of context
        c) STRUCTURE specification: [node's name] + [all parents' names]
           E.g. sneeze cold allergy
           One restriction here is you must specify the structure information
           the same sequence you specify node. E.g
                    cat N Y
                    cold N Y
                    allergy N Y
                    sneeze N Y
                    #
           The structure specification MUST adhere to the following order:
                    cat 
                    cold
                    allergy cat
                    sneeze cold allergy
    ---------------------------------
    2. Training Data Specification:
       EXAMPLE FILE: trainingData
    ---------------------------------
        a) "?" is specially reserved for unknown entry
        b) feature sequence (aka column from left to right):
           Must follow the same order as NODE and STRUCTURE specification
                    cat     cold    allergy     sneeze
        c) feature states: must use the same string as in NODE specification

    --------------------------
    3. Queries:
       EXAMPLE FILE: myQuery
    --------------------------
    You can put as many queries as you want in this file. Different queries are
    separated by "#"
    Each query consists of two lines:
        a) first line: evidence you have
        b) second line: your question
    E.g:    
            cold N sneeze Y
            cat Y
    is querying P(cat=Y | cold=N, sneeze=Y)

=============
How To Run:
=============
    1) untar the file "tar xvf bayesEM.tar"
    2) change to "src" and type "make"
    3) "./cat" 
    One thing to note is this program is not hard coded for "cat" problem, it is
    a generic method for Bayesian Network Parameter learning and inference. 
    For smaller problem, it is worth trying to run this program

========================
Possible Improvements:
========================
    1) For training data set, definitely should use database to improve
    efficiency. Even for storing CPDs, should consider using database.
    2) Input requirements is kind of strict, it can be loosen.
\end{verbatim}

\end{document}
