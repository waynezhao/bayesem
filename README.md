Expectation Maximization (EM) for learning Conditional Probability Distribution
(CPD) of Bayesian Network from incomplete data.

Small project from Statistical Relational Learning (B659) class. Spring 2014 at IUB