// ----------------------------------------------------------------------------
// A class for Bayesian Network
//========================================
// Weiran Zhao, Computer Science Dept
// Indiana University, Bloomington
//========================================
// Started: Wed,Apr 02th 2014 10:25:47 AM EDT
// Last Modified: Thu,Apr 03th 2014 01:32:52 AM EDT
// ----------------------------------------------------------------------------
#ifndef _BAYES_NET__H_
#define _BAYES_NET__H_
#include"bayesNode.h"

class bayesNet {
    protected:
        int numNodes;                           // number of nodes
        vector<bayesNode> allNodes;             // all the nodes of Bayesian Network
        vector<vector<int> > compData;          // complete data, data tuples with no missing values
        vector<vector<int> > missData;          // incomplete data, data tuples with missing values
        vector<int> missDataLineNumber;         // line number of missing data (count from 0)
        vector<vector<int> > curFillData;       // currently filled data using current JPD
        vector<string> index2Names;             // given index, return name of corresponding node
        map<string, int> name2Index;            // given node name, return its index
        vector<vector<string> > index2States;   // given index, return index--> name state map of corresponding node
        vector<map<string, int> > states2Index; // given index, return name--> index state map of corresponding node
        
        // given a state sequence, return its joint probability
        double queryJPD(vector<int> seq);
        // learn cpd of each node given data (all or partial)
        void learnAllCPD(vector<vector<int> > data); 
        // fill missing data using current JPD
        vector<vector<int> > fillMissingData(vector<vector<int> > data);
        // data equal if given set of 2D data are the same
        bool dataEqual(vector<vector<int> > dt1, vector<vector<int> > dt2);
        // merge two data
        vector<vector<int> > mergeData(vector<vector<int> > dt1, vector<vector<int> > dt2);
        // print Query result
        void printQueryResult();
        //---------------------------------------------------------------------
        // given a query string (e.g. cat Y cold Y), generate its corresponding
        // state vector (e.g. {1, -1, 1, -1})
        //---------------------------------------------------------------------
        vector<int> string2QVec(string s);
        // string with equal. given (cat Y cold Y) return (cat = Y, cold = Y)
        string stringWithEq(string s);

    public:
        // constructor
        bayesNet(string specFile);
        // load data from file and split them based on missing data
        void loadData(string dataFile);
        // training bayesian network using EM
        void EMTraining();
        // given a query in file queryFile, output result
        void query(string queryFile);
        // print CPD
        void printAllCPD();

};

#endif
