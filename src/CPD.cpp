// ----------------------------------------------------------------------------
// A class for representing CPD (Conditional Probability Distribution) 
//========================================
// Weiran Zhao, Computer Science Dept
// Indiana University, Bloomington
//========================================
// Started: Wed,Apr 02th 2014 10:28:39 AM EDT
// Modified: Wed,Apr 02th 2014 08:58:02 PM EDT
//           Bug fixed for normalizeCPD() when dim==1
// Last Modified: Wed,Apr 02th 2014 10:30:57 PM EDT
// ----------------------------------------------------------------------------
#include"CPD.h"
#include"utils.h"
#include<iostream>
#include<algorithm>

CPD::CPD(const vector<int>& nspd, int vCnt) {
    //------------------------------
    // class variable initialization
    //------------------------------
    virtualCount = vCnt;
    dim = nspd.size();
    numStatesPerDim = nspd;
    // initialize count and CPD
    clear();
    // normalize CPD
    normalizeCPD();
}

void CPD::normalizeCPD() {
    //-------------------------------------------------------
    // if there's no parent, just divide by the sum of itself
    //-------------------------------------------------------
    if(dim==1) {
        //get sum
        double sum=0;
        map<vector<int>, int>::iterator it;
        for(it = count.begin(); it != count.end(); it++) {
            sum+= it->second;
        }
        // divided by sum
        for(it = count.begin(); it != count.end(); it++) {
            cpd[it->first] = count[it->first]/sum;
        }
    } else {
        //-------------------------------------------
        // first sum over the last feature (variable)
        //-------------------------------------------
        map<vector<int>, int> sumCount;
        // create keys for sumCount, keys are of length dim-1
        vector<int> tmp;
        for(int i=0;i<dim-1;i++) {
            tmp.push_back(-1);
        }
        // states per dim 
        vector<int> nspd = numStatesPerDim;
        nspd.pop_back();
        vector<vector<int> > keys = generatePossibleSeqs(tmp, nspd);
        // create sum counts
        for(int i=0;i<keys.size();i++) {
            sumCount[keys[i]] = 0;
            tmp = keys[i];
            tmp.push_back(-1);
            // sum over last feature (variable)
            for(int j=0;j<numStatesPerDim[dim-1];j++) {
                tmp[dim-1] = j;
                sumCount[keys[i]] += queryCount(tmp);
            }
        }

        //----------------------------------------
        // normalize by dividing count by sumCount
        //----------------------------------------
        map<vector<int>, int>::iterator cntIter, sumIter;
        vector<int> tmpCPDKey, tmpSumKey;
        for(cntIter = count.begin(); cntIter!= count.end(); ++cntIter) {
            // cpd's key should be same as count's
            tmpCPDKey = cntIter->first;
            // create sumCount's key by delete last state
            tmpSumKey = tmpCPDKey; tmpSumKey.pop_back();
            sumIter = sumCount.find(tmpSumKey);
            // in case divided by 0, use max()
            cpd[tmpCPDKey] = double(cntIter->second)/max(1, (sumIter->second));
        }
    }
}

int CPD::queryCount(vector<int> seq) {
    map<vector<int>, int>::iterator it;
    it = count.find(seq);
    return it->second;
}

void CPD:: clear() {
    //----------------------------------
    // let's create count and cpd's keys
    //----------------------------------
    vector<int> tmp;
    for(int i=0;i<dim;i++) {
        tmp.push_back(-1);
    }
    vector<vector<int> > keys = generatePossibleSeqs(tmp, numStatesPerDim);
    //-------------------------
    // initialize count and cpd
    //-------------------------
    for(int i=0;i<keys.size();i++) {
        count[keys[i]] = virtualCount;
        cpd[keys[i]] = 0.0;
    }
}

void CPD::printCount(map<vector<int>, int>& intTable) {
    map<vector<int>, int>::iterator it;
    for(it = intTable.begin();it!= intTable.end();++it) {
        print1DVector(it->first);
        cout<<"|\t";
        cout<<it->second<<endl;
    }
}

void CPD::printCPD() {
    map<vector<int>, double>::iterator it;
    for(it = cpd.begin();it!= cpd.end();++it) {
        print1DVector(it->first);
        cout<<"|\t";
        cout<<it->second<<endl;
    }
}
