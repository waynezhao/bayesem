// ----------------------------------------------------------------------------
// This file is for utilities functions used by bayesNode and/or bayesNet class
//========================================
// Weiran Zhao, Computer Science Dept
// Indiana University, Bloomington
//========================================
// Started: Tue,Apr 01th 2014 09:56:14 PM EDT
// Last Modified: Wed,Apr 02th 2014 10:40:50 PM EDT
// ----------------------------------------------------------------------------
#ifndef _UTILS__H__
#define _UTILS__H__
#include<vector>

using namespace std;

//---------------------------
// Define some constants here
//---------------------------
// EM method max iteration
#define EM_MAX_ITER 10
// Virtual count when learning CPDs
#define VIRTUAL_CNT 1

//----------------------------------------------------------------------------
// Assume in the domain of sneezing ({cold, allergy, sneezing}).
//-------
// Given:
//-------
//      1) givenSeq: A sequence (e.g. {0, -1, 1}) indicating the state of each
//      variable;
//      2) numStates: A sequence (e.g. {2, 2, 2}) indicating number of possible
//      states of each variable;
// P.S. : "-1" indicating the state of (allergy) is unknown
//--------
// Output:
//--------
//      All possible sequences of state. e.g.
//      {0, 0, 1}, {0, 1, 1}
//----------------------------------------------------------------------------
vector<vector<int> > generatePossibleSeqs(vector<int> givenSeq, vector<int> numStates);

//--------------------------------------------------------------------
// print 2D vector of int (Helper function for generatePossibleSeqs())
//--------------------------------------------------------------------
void print2DVector(vector<vector<int> > states);
//--------------------------------------------------------------------
// print 1D vector of int (Helper function for generatePossibleSeqs())
// Do NOT print EOL (end of line)
//--------------------------------------------------------------------
void print1DVector(vector<int> seq);
#endif
