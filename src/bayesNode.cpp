// ----------------------------------------------------------------------------
// A class for representing a node of Bayesian Network
//========================================
// Weiran Zhao, Computer Science Dept
// Indiana University, Bloomington
//========================================
// Started: Wed,Apr 02th 2014 10:27:21 AM EDT
// Last Modified: Thu,Apr 03th 2014 12:33:51 AM EDT
// ----------------------------------------------------------------------------
#include"bayesNode.h"
#include"utils.h"
#include<iostream>
#include<cassert>
#include<map>
#include<iomanip>

bayesNode::bayesNode(string n, vector<int>& l2gMap, vector<string>& gidx2nm, vector<vector<string> >& gidx2sts, vector<int>& nspd, int vCnt):CPD(nspd, vCnt) {
    name = n;
    local2Global = l2gMap;
    gIdx2Names = gidx2nm;
    gIdx2States = gidx2sts;
}

void bayesNode::learnCPD(vector<vector<int> > data) {
    vector<int> queryStates;
    map<vector<int>,int>::iterator it;
    //--------------------
    // reset count and cpd
    //--------------------
    this->clear();
    for(int i=0;i<data.size();i++) {
        queryStates.clear();
        //--------------------
        // create query vector
        //--------------------
        for(int j=0;j<dim;j++) {
            queryStates.push_back(data[i][local2Global[j]]);
        }
        //----------------------------------------------
        // this way is safer than "count[queryStates]++"
        //----------------------------------------------
        it = count.find(queryStates);
        assert(it!=count.end());
        (it->second)++;
    }
    normalizeCPD();
}

double bayesNode::queryCPD(vector<int> seq) {
    //------------------------------
    // trim seq to fit for this node
    //------------------------------
    vector<int> trimmedSeq;
    for(int i=0;i<dim;i++) {
        trimmedSeq.push_back(seq[local2Global[i]]);
    }

    //-------------------------------------------
    // get the value using this trimmedSeq as key
    //-------------------------------------------
    map<vector<int>, double>::iterator it;
    it = cpd.find(trimmedSeq);
    assert(it!=cpd.end());

    return it->second;
}

void bayesNode::printCPD() {
    //------------------------
    // print title of this CPD
    //------------------------
    cout<<"----------------------------------------------------------------------------"<<endl;
    cout<<"CPD of node: "<<name<<endl;
    for(int i=0; i<dim; i++) {
        cout<<"|\t"<<gIdx2Names[local2Global[i]]<<"\t";
    }
    cout<<"|\tprob\t|"<<endl;
    //------------------------------------
    // print values and states of this CPD
    //------------------------------------
    map<vector<int>, double>::iterator it;
    vector<int> tmpKey;
    for(it=cpd.begin(); it!=cpd.end();it++) {
        tmpKey = it->first;
        // print key (which is states sequence)
        for(int i=0;i<tmpKey.size();i++) {
            cout<<"|\t"<<gIdx2States[local2Global[i]][tmpKey[i]]<<"\t";
        }
        cout<<"|\t"<<setprecision(3)<<it->second<<"\t|"<<endl;
    }
}
