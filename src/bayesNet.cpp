// ----------------------------------------------------------------------------
// A class for Bayesian Network
//========================================
// Weiran Zhao, Computer Science Dept
// Indiana University, Bloomington
//========================================
// Started: Wed,Apr 02th 2014 10:25:47 AM EDT
// Last Modified: Thu,Apr 03th 2014 02:29:06 AM EDT
// ----------------------------------------------------------------------------
#include"bayesNet.h"
#include"utils.h"
#include<fstream>
#include<cstdlib>
#include<iostream>
#include<iomanip>
#include<sstream>
#include<cassert>

bayesNet::bayesNet(string specFile) {
    //----------------------------------
    // open bayes net specification file
    //----------------------------------
    ifstream specsIn(specFile.c_str());
    if(!specsIn.is_open()) {
        cerr<<"File: "<<specFile<<" not open"<<endl;
        abort();
    }
    //----------------------------------------------------
    // read in names of all nodes and corresponding states
    //----------------------------------------------------
    stringstream line;
    string tmp;
    vector<string> tmpIdx2States;
    map<string, int> tmpStates2idx;
    while(getline(specsIn, tmp)) {
        if(tmp.compare("#")==0) {
            // we finish processing names and states of nodes
            break;
        }
        // set string stream of line
        line.clear();
        line.str(tmp);
        // record index-->name and name-->index
        line >> tmp;
        name2Index.insert(pair<string, int>(tmp, index2Names.size()));
        index2Names.push_back(tmp);
        // takes care of states of node
        tmpIdx2States.clear();
        tmpStates2idx.clear();
        while(line>>tmp) {
            tmpStates2idx.insert(pair<string, int>(tmp, tmpIdx2States.size()));
            tmpIdx2States.push_back(tmp);
        }
        states2Index.push_back(tmpStates2idx);
        index2States.push_back(tmpIdx2States);
    }
    //-----------------------
    // assign number of nodes
    //-----------------------
    numNodes = index2Names.size();
    //----------------------------------------
    // read in structure information (parents)
    //----------------------------------------
    vector<int> parentInds;
    vector<int> nspd;       // number of states per dimension
    int selfIdx;
    map<string, int>::iterator it;
    while(getline(specsIn, tmp)) {
        // set string stream
        line.clear();
        line.str(tmp);
        // get node name 
        line >> tmp;
        selfIdx = name2Index[tmp];
        parentInds.clear();
        nspd.clear();
        while(line >> tmp) {
            //----------------------------------------------------------------
            // This is a safer way to work than "int tmpIdx = name2Index[tmp]"
            //----------------------------------------------------------------
            it = name2Index.find(tmp);
            assert(it!=name2Index.end());
            parentInds.push_back(it->second);
            nspd.push_back(index2States[it->second].size());
        }
        // add self dim to nspd and parentInds
        nspd.push_back(index2States[selfIdx].size());
        parentInds.push_back(selfIdx);
        //--------------------------------------
        // bayesian network node is created here
        //--------------------------------------
        allNodes.push_back(bayesNode(index2Names[selfIdx], parentInds, index2Names, index2States, nspd, VIRTUAL_CNT));
    }
    //-----------
    // close file
    //-----------
    specsIn.close();
}

void bayesNet::loadData(string dataFile) {
    //------------------------
    // open training data file
    //----------------------------------
    ifstream dataIn(dataFile.c_str());
    if(!dataIn.is_open()) {
        cerr<<"File: "<<dataFile<<" not open"<<endl;
        abort();
    }
    //----------------------------------------
    // reading data and convert and split them
    //----------------------------------------
    stringstream line;
    string tmp;
    vector<int> toInsert;
    bool complete;
    int counter; 
    int currentLine = 0;
    map<string,int>::iterator it;
    // loop through those data
    while(getline(dataIn, tmp)) {
        // reset variables
        toInsert.clear();
        line.clear();
        complete = true;
        counter = 0;
        line.str(tmp);
        while(line>>tmp) {
            if(tmp.compare("?")==0) {
                toInsert.push_back(-1);
                complete=false;
            } else {
                //----------------------------------------------------------
                // this is a safer way than push "states2Index[counter][tmp]
                //----------------------------------------------------------
                it = states2Index[counter].find(tmp);
                assert(it!=states2Index[counter].end());
                toInsert.push_back(it->second);
            }
            counter++;
        }
        //---------------------------
        // now decide where to insert
        //---------------------------
        if(complete) {
            compData.push_back(toInsert);
        } else {
            missData.push_back(toInsert);
            missDataLineNumber.push_back(currentLine);
        }
        currentLine++;
    }
    
    //-----------
    // close file
    //-----------
    dataIn.close();
}

void bayesNet::learnAllCPD(vector<vector<int> > data) {
    //------------------------------
    // let each know learn their CPD
    //------------------------------
    for(int i=0; i<numNodes; i++) {
        allNodes[i].learnCPD(data);
    }
}

void bayesNet::EMTraining() {
    //--------------------------------------------------------
    // let's have a initial guess using existing complete data
    //--------------------------------------------------------
    learnAllCPD(compData);
    //--------------------------------------------
    // E-M till max iteration or early termination
    //--------------------------------------------
    int cnter = 0;
    vector<vector<int> > newFilledData;
    vector<vector<int> > allData;
    curFillData = missData;
    while(cnter<EM_MAX_ITER) {
        //-------------------------------------------------------------
        // Expectation step (aka fill missing value of incomplete data)
        //-------------------------------------------------------------
        newFilledData = fillMissingData(missData);

        // check if we need to do early termination
        if(dataEqual(curFillData, newFilledData)) {
            cout<<"EM early terminated with "<<cnter+1<<" iterations"<<endl;
            break;
        }
        curFillData = newFilledData;

        // learn cpd again using compData+fillData
        allData = mergeData(curFillData, compData);
        learnAllCPD(allData);

        // update counter
        cnter++;
    }

    // print cpd
    printAllCPD();

    // print filled data
    cout<<"----------------------------------------------------------------------------"<<endl;
    cout<<"filled data"<<endl;
    for(int i=0;i<curFillData.size();i++) {
        cout<<"Line #"<<missDataLineNumber[i]+1<<":\t";
        for(int j=0; j<curFillData[i].size(); j++) {
            cout<<index2States[j][curFillData[i][j]]<<"\t";
        }
        cout<<endl;
    }
}

double bayesNet::queryJPD(vector<int> seq) {
    double jpd = 1.0;
    //---------------------------------
    // just a multiplication of all cpd
    //---------------------------------
    for(int i=0;i<numNodes;i++) {
        jpd*= allNodes[i].queryCPD(seq);
    }
    return jpd;
}

vector<vector<int> > bayesNet::fillMissingData(vector<vector<int> > data) {
    vector<vector<int> > filledData;
    vector<vector<int> > possibleSeqs;
    vector<int> bestFit;
    double highestProb, prob;
    vector<int> nspd;
    for(int i=0; i<numNodes; i++) {
        nspd.push_back(index2States[i].size());
    }
    for(int i=0; i<data.size(); i++) {
        // first generate all possible sequence
        possibleSeqs = generatePossibleSeqs(data[i], nspd);
        //--------------------------------------
        // pick the one with highest probability
        //--------------------------------------
        highestProb = -1.0;
        for(int j=0; j<possibleSeqs.size();j++) {
            prob = queryJPD(possibleSeqs[j]);
            if(prob > highestProb) {
                highestProb = prob;
                bestFit = possibleSeqs[j];
            }
        }
        // push back bestFit
        filledData.push_back(bestFit);
    }

    return filledData;
}

bool bayesNet::dataEqual(vector<vector<int> > dt1, vector<vector<int> > dt2) {
    // check if size of 1st dim the same
    if(dt1.size()!=dt2.size()) {
        return false;
    }
    for(int i=0;i<dt1.size();i++) {
        // check size of 2nd dim
        if(dt1[0].size()!=dt2[0].size()) {
            return false;
        } 
        // check each corresponding values
        for(int j=0; j<dt1[i].size(); j++) {
            if(dt1[i][j]!=dt2[i][j]) {
                return false;
            }
        }
    }
    return true;
}

vector<vector<int> > bayesNet::mergeData(vector<vector<int> > dt1, vector<vector<int> > dt2) {
    vector<vector<int> > allData = dt1;
    for(int i=0; i< dt2.size();i++) {
        allData.push_back(dt2[i]);
    }
    return allData;
}

void bayesNet::printAllCPD() {
    for(int i=0;i<numNodes;i++) {
        allNodes[i].printCPD();
    }
}

vector<int> bayesNet::string2QVec(string s) {
    stringstream ss(s);
    //---------------------------------------
    // create a int vector of size 'numNodes'
    //---------------------------------------
    int *states = (int*) malloc(sizeof(int)*numNodes);
    for(int i=0;i<numNodes;i++) {
        states[i] = -1;
    }
    //-------------------
    // fill in the states
    //-------------------
    map<string, int>::iterator it1, it2;
    string tmp1, tmp2;
    while((ss>>tmp1) && (ss>>tmp2)) {
        it1 = name2Index.find(tmp1);
        assert(it1!=name2Index.end());
        it2 = states2Index[it1->second].find(tmp2);
        assert(it2!=states2Index[it1->second].end());
        states[it1->second] = it2->second;
    }

    vector<int> result(states, states+numNodes);
    //------------
    // free memory
    //------------
    free(states);

    return result;
}

void bayesNet::query(string queryFile) {
    //----------------
    // open query file
    //----------------
    ifstream queryIn(queryFile.c_str());
    if(!queryIn.is_open()) {
        cerr<<"File: "<<queryFile<<" not open"<<endl;
        abort();
    }
    //----------------------------
    // fulfill every query in file
    //----------------------------
    string given, target, tmp;
    while( (getline(queryIn, given)) && (getline(queryIn, target))) {
        // print Query
        cout<<"----------------------------------------------------------------------------"<<endl;
        cout<<"Query: P("<<stringWithEq(target)<<" | "<<stringWithEq(given)<<")";
        //---------------------------
        // calculate that probability
        //---------------------------
        target = target+" "+given;
        double pGiven = 0, pTarget = 0;
        //-----------------------------
        // calculate pGiven and pTarget
        //-----------------------------
        vector<int> tmpSeq;
        vector<vector<int> > possSeqs;
        vector<int> nspd;
        for(int i=0;i<numNodes;i++) {
            nspd.push_back(index2States[i].size());
        }
        // pGiven
        tmpSeq = string2QVec(given);
        possSeqs = generatePossibleSeqs(tmpSeq, nspd);
        for(int i=0;i<possSeqs.size();i++) {
            pGiven += queryJPD(possSeqs[i]);
        }
        // pTarget 
        tmpSeq = string2QVec(target);
        possSeqs = generatePossibleSeqs(tmpSeq, nspd);
        for(int i=0;i<possSeqs.size();i++) {
            pTarget += queryJPD(possSeqs[i]);
        }
        cout<<" = "<<setprecision(4)<<pTarget/pGiven<<endl;
        cout<<"Numerator is "<<pTarget<<", Denominator is "<<pGiven<<endl;

        //----------------------
        // decide if we continue
        //----------------------
        getline(queryIn, tmp);
        if(tmp.compare("#")==0) {
            continue;
        } else {
            break;
        }
    }
}

string bayesNet::stringWithEq(string s) {
    stringstream ss(s);
    string result("");
    string tmp1, tmp2;
    bool first = true;
    while( (ss>>tmp1) && (ss>>tmp2)) {
        if( !first ) {
            result += ", ";
        } else {
            first = false;
        }
        result += tmp1;
        result += " = ";
        result += tmp2;
    }

    return result;
}
