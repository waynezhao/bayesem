// ----------------------------------------------------------------------------
// A class for representing a node of Bayesian Network
//========================================
// Weiran Zhao, Computer Science Dept
// Indiana University, Bloomington
//========================================
// Started: Wed,Apr 02th 2014 10:27:21 AM EDT
// Last Modified: Thu,Apr 03th 2014 12:13:49 AM EDT
// ----------------------------------------------------------------------------
#ifndef _BAYES_NODE__H__
#define _BAYES_NODE__H__
#include"CPD.h"
#include<string>
#include<vector>

class bayesNode: public CPD {
    protected:
        string name;                        // name of this node
        vector<int> local2Global;           // local index to global index map
        vector<string> gIdx2Names;          // index 2 names global
        vector<vector<string> > gIdx2States;// index to state names global


    public:
        // constructor
        bayesNode(string n, vector<int>& l2gMap, vector<string>& gidx2nm, vector<vector<string> >& gidx2sts, vector<int>& nspd, int vCnt=1);
        // learn CPD from data
        void learnCPD(vector<vector<int> > data);
        //-----------------------------------------------------------
        // given a state sequence return the conditional probability
        // seq is a state sequece of all features, need to be trimmed
        //-----------------------------------------------------------
        double queryCPD(vector<int> seq);
        // print
        void printCPD();
};

#endif
