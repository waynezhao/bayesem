#include"utils.h"
#include<cassert>
#include<iostream>

vector<vector<int> > generatePossibleSeqs(vector<int> givenSeq, vector<int> numStates) {
    //---------------
    // input checking
    //---------------
    assert(givenSeq.size()==numStates.size());

    //------------------
    // declare variables
    //------------------
    vector<vector<int> > possibleSeqs, tmpSeqs;
    vector<int> tmp;
    tmpSeqs.push_back(givenSeq);

    // check each position of each state sequence
    for(int i=0;i<givenSeq.size();i++) {
        // if the ith state is fix, no need to expand
        if(tmpSeqs[0][i]!=-1) {
            possibleSeqs = tmpSeqs;
            continue;
        } else { // expand -1 to {0, 1, 2, ... numStates[i]} 
            // clear possibleSeqs
            possibleSeqs.clear();
            // for each state sequence, expand -1
            for(int j=0;j<tmpSeqs.size();j++) {
                tmp = tmpSeqs[j];
                for(int k=0; k<numStates[i];k++) {
                    tmp[i] = k;
                    possibleSeqs.push_back(tmp);
                }
            }
            // assign possibleSeqs to tmpSeqs
            tmpSeqs = possibleSeqs;
        }
    }

    return possibleSeqs;
}

void print2DVector(vector<vector<int> > states) {
    for(int i=0;i<states.size();i++) {
        for(int j=0;j<states[i].size();j++) {
            cout<<states[i][j]<<"\t";
        }
        cout<<endl;
    }
}

void print1DVector(vector<int> seq) {
    for(int i=0;i<seq.size();i++) {
        cout<<seq[i]<<"\t";
    }
    //cout<<endl;
}

//int main() {
//    cout<<"============================================================"<<endl;
//    vector<int> givenSeq;
//    givenSeq.push_back(0); 
//    givenSeq.push_back(-1); 
//    givenSeq.push_back(-1);
//    vector<int> numStates(3,3);
//    vector<vector<int> > possSeqs = generatePossibleSeqs(givenSeq,numStates);
//    cout<<"givenSeq:"<<endl;
//    print1DVector(givenSeq); cout<<endl;
//    cout<<"numStates:"<<endl;
//    print1DVector(numStates); cout<<endl;
//    cout<<"possSeqs:"<<endl;
//    print2DVector(possSeqs);
//
//    return 0;
//}
