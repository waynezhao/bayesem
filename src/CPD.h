// ----------------------------------------------------------------------------
// A class for representing CPD (Conditional Probability Distribution) 
//========================================
// Weiran Zhao, Computer Science Dept
// Indiana University, Bloomington
//========================================
// Started: Wed,Apr 02th 2014 10:28:39 AM EDT
// Last Modified: Wed,Apr 02th 2014 03:18:27 PM EDT
// ----------------------------------------------------------------------------
#ifndef _CPD__H__
#define _CPD__H__

#include<vector>
#include<map>

using namespace std;

class CPD {
    protected:
        int virtualCount;                   // prior virtual count to all states of variables
        int dim;                            // number of features for this CPD table
        vector<int> numStatesPerDim;        // number of possible states per dimension
        map<vector<int>, int> count;        // count for every possible combinations of states
        map<vector<int>, double> cpd;       // cpd vector<int> serves as keys

        // calculate cpd from count
        void normalizeCPD();                     
        // get count given a state sequence
        int queryCount(vector<int> seq);

    public:
        // constructor
        CPD(const vector<int>& nspd, int vCnt=1);
        // query CPD given a state sequence
        double queryCPD(vector<int> seq);
        // reset (initialize) cpd and count
        void clear();                            
        //--------------------------------
        // Need to do a pretty print later
        //--------------------------------
        // print count 
        void printCount(map<vector<int>, int>& intTable);
        // print CPD
        void printCPD();
};

#endif
