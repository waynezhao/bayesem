#include<iostream>
#include<fstream>
#include<sstream>
#include<map>
#include<vector>

#include"CPD.h"
#include"bayesNet.h"
#include"utils.h"

using namespace std;

int main() {
    string specs("bayesNetSpecs");
    string data("trainingData");
    string myQuery("myQuery");
    // create bayesian network
    bayesNet myNet(specs);
    // load training data
    myNet.loadData(data);
    // learning parameters using EM
    myNet.EMTraining();
    // fulfill my query
    myNet.query(myQuery);
    return 0;
}
